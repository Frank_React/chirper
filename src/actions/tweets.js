import { saveLikeToggle, saveTweet } from "../utils/api";
import { showLoading, hideLoading } from 'react-redux-loading';
export const RECEIVE_TWEETS = "RECEIVE_TWEETS"; // the action type when we receive a dispatch to update the store
export const TOGGLE_TWEET = "TOGGLE_TWEET";
export const ADD_TWEET = 'ADD_TWEET';

function addTweet(tweet){
    return {
        type: ADD_TWEET,
        tweet,
    }
}

export function handleAddTweet (text, replyingTo){
    return (dispatch, getState) => {
        const { authedUser } = getState();
        dispatch(showLoading()); // show that it's loading
        // saveTweet is a funtion from the server API
        return saveTweet({
            text,
            author: authedUser,
            replyingTo
        })
            .then((tweet) => dispatch(addTweet((tweet)))) // Once this is done then add it to our own state
            .then(() => dispatch(hideLoading())) // now we hide the loading
    }
}

export function receiveTweets(tweets){
    return {
        type: RECEIVE_TWEETS,
        tweets: tweets
    }
}
// This creates the proper action object for the reducer
export function toggleTweet({id, authedUser, hasLiked}){
    return {
        type: TOGGLE_TWEET,
        id,
        authedUser,
        hasLiked
    }
}

export function handleToggleTweet(info){
    return (dispatch) => {
        dispatch(toggleTweet(info));

        return saveLikeToggle(info).catch((e) => {
            console.warn("Error in handleToggleTweet: ", e);
            dispatch(toggleTweet(info));
            alert('Whoops something went wrong when you liked that tweet. Could you try again?')
        })
    }
}