import React, { Component } from 'react';
import { connect } from 'react-redux';
import { handleAddTweet } from "../actions/tweets";
import { Redirect } from 'react-router-dom';

class NewTweet extends Component {
    /* This going to be a control component so React will be in control of the input so that submit is allowed when
    something is added. Whenever you are updating UI based on state of component you want to use controlled component  */
    state = {
        text: '',
        toHome: false
    };

    handleChange = (e) => {
        const text = e.target.value;
        this.setState(() => ({
            text: text
        }))
    };

    handleSubmit = (e) => {
        e.preventDefault();
        const { text } = this.state;
        const { dispatch, id } = this.props; // We will pass in an id if it's a reply
        dispatch(handleAddTweet(text, id));
        this.setState({
            text: '',
            toHome: id ? false : true, // if id is null then it's a stand alone tweet component and submit should go to home
        })
    };
    render(){
        const {text, toHome} = this.state;
        const tweetLeft = 280 - text.length;
        // CASE 0: toHome is true and we want to back to home
        if (toHome === true){
            return <Redirect to ='/'/>
        }
        // CASE 1: toHome is false then render the NewTweet component
        else {
            return(
                <div>
                <h3 className = 'center'> Compose new Tweet </h3>
                <form className = 'new-tweet' onSubmit={this.handleSubmit}>
                <textarea
                    placeholder= "What's going on man?"
                    value = {text}
                    onChange = {this.handleChange}
                    className = 'textarea'
                    maxLength = {280}
                />
                {
                    tweetLeft <= 100 && (
                        <div className = 'tweet-length'>
                            {tweetLeft}
                        </div>
                    )
                }
                <button className = 'btn' type = 'submit' disabled = {text === ''}>
                    Submit
                </button>
                </form>
            </div>
            )
        }

    }
}

export default connect()(NewTweet);