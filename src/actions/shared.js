import { getInitialData } from "../utils/api";
import { receiveTweets } from "./tweets";
import { receiveUsers } from "./users";
import { setAuthedUser} from "./authedUser";
import { showLoading, hideLoading } from 'react-redux-loading'
const AUTHED_ID = 'mia'; // to keep things simple we are just using this constant

// This will get all the available data
export function handleInitialData(){
    return (dispatch) => {
        dispatch(showLoading()); // Show the loading at the start
        return getInitialData()
            .then(({users, tweets}) => {
                // take users and tweets and add them to the state of our Redux store
                // this will require us to dispatch a few different actions
                dispatch(receiveTweets(tweets));
                dispatch(receiveUsers(users));
                dispatch(setAuthedUser(AUTHED_ID));
                dispatch(hideLoading()); // hide loading at the end
            })
    }
}